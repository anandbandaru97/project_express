const express = require("express");
const app = express();
const port = process.env.PORT || 3000;

app.use(express.json());

// Routes
const userRoute = require("./routes/userRoute");
const projectRoute = require("./routes/projectRoute");
const todoRoute =  require("./routes/todoRoute")

app.use("/api/user", userRoute);
app.use("/api/project", projectRoute);
app.use("/api/todo", todoRoute)

// Start the Express server
app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});

