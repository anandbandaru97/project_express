const express = require("express");
const router = express.Router();
const toDoTask = require("../tasks/todoTask");

// Create a new To-Do (Create)
router.post("/", async (req, res) => {
  const { title, status, project_id } = req.body;
  if (!title || !project_id) {
    return res.status(400).json({ error: "Invalid To-Do data" });
  }

  try {
    const newToDoId = await toDoTask.createToDoAsync(title, status || "not done", project_id);
    res.json({ message: "To-Do created successfully", toDoId: newToDoId });
  } catch (error) {
    console.error(error.message);
    res.status(500).json({ error: "Internal Server Error" });
  }
});

// Get all To-Do items (Read)
router.get("/", async (req, res) => {
  try {
    const toDos = await toDoTask.getAllToDosAsync();
    res.json(toDos);
  } catch (error) {
    console.error(error.message);
    res.status(500).json({ error: "Internal Server Error" });
  }
});

// Get a single To-Do item by ID (Read)
router.get("/:id", async (req, res) => {
  const { id } = req.params;
  try {
    const toDo = await toDoTask.getToDoByIdAsync(id);
    if (!toDo) {
      return res.status(404).json({ error: "To-Do not found" });
    }
    res.json(toDo);
  } catch (error) {
    console.error(error.message);
    res.status(500).json({ error: "Internal Server Error" });
  }
});

// Update a To-Do item (Update)
router.put("/:id", async (req, res) => {
  const { id } = req.params;
  const { title, status, project_id } = req.body;

  try {
    await toDoTask.updateToDoAsync(id, title, status, project_id);
    res.json({ message: "To-Do updated successfully" });
  } catch (error) {
    console.error(error.message);
    if (error.message === "To-Do not found") {
      return res.status(404).json({ error: "To-Do not found" });
    }
    res.status(500).json({ error: "Internal Server Error" });
  }
});

// Delete a To-Do item by ID (Delete)
router.delete("/:id", async (req, res) => {
  const { id } = req.params;

  try {
    await deleteToDoAsync(id);
    res.json({ message: "To-Do deleted successfully" });
  } catch (error) {
    console.error(error.message);
    if (error.message === "To-Do not found") {
      return res.status(404).json({ error: "To-Do not found" });
    }
    res.status(500).json({ error: "Internal Server Error" });
  }
});

module.exports = router;
