const express = require("express");
const router = express.Router();
const ProjectTask = require("../tasks/projectTask");

// Create a new project
router.post("/", async (req, res) => {
  const { title, description, user_id } = req.body;
  if (!title || !user_id) {
    return res.status(400).json({ error: "Invalid project data" });
  }

  try {
    const projectId = await ProjectTask.createProject(title, description, user_id);
    res.json({ message: "Project created successfully", projectId });
  } catch (error) {
    console.error(error.message);
    res.status(500).json({ error: "Internal Server Error" });
  }
});

// Get project by projectId
router.get("/:projectId", async (req, res) => {
  const { projectId } = req.params;
  try {
    const project = await ProjectTask.getProjectById(projectId);
    if (!project) {
      return res.status(404).json({ error: "Project not found" });
    }
    res.json(project);
  } catch (error) {
    console.error(error.message);
    res.status(500).json({ error: "Internal Server Error" });
  }
});

router.get("/", async (req, res) => {
    try {
        const projects = await ProjectTask.getAllProjects();
        res.json(projects)
    } catch (error) {
        console.error(error.message);
        res.status(500).json({error: "Internal Server Error"})
    }
})

// Update a project
router.put("/:projectId", async (req, res) => {
  const { projectId } = req.params;
  const { title, description } = req.body;
  if (!title) {
    return res.status(400).json({ error: "Invalid project data" });
  }

  try {
    await ProjectTask.updateProject(projectId, title, description);
    res.json({ message: "Project updated successfully" });
  } catch (error) {
    console.error(error.message);
    res.status(500).json({ error: "Internal Server Error" });
  }
});

// Delete a project
router.delete("/:projectId", async (req, res) => {
  const { projectId } = req.params;
  try {
    await ProjectTask.deleteProject(projectId);
    res.json({ message: "Project deleted successfully" });
  } catch (error) {
    console.error(error.message);
    res.status(500).json({ error: "Internal Server Error" });
  }
});

module.exports = router;
