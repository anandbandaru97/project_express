const express = require("express");
const router = express.Router();
const UserModel = require("../tasks/userTask");

// Create a new user
router.post("/", async (req, res) => {
  const { username, email, password } = req.body;
  if (!username || !email || !password) {
    return res.status(400).json({ error: "Invalid user data" });
  }

  try {
    const userId = await UserModel.createUser(username, email, password);
    res.json({ message: "User created successfully", userId });
  } catch (error) {
    console.error(error.message);
    res.status(500).json({ error: "Internal Server Error" });
  }
});

// Get all users
router.get("/", async (req, res) => {
  try {
    const users = await UserModel.getAllUsers();
    res.json(users);
  } catch (error) {
    console.error(error.message);
    res.status(500).json({ error: "Internal Server Error" });
  }
});

// Get user by username
router.get("/:username", async (req, res) => {
  const { username } = req.params;
  try {
    const user = await UserModel.getUserByUsername(username);
    if (!user) {
      return res.status(404).json({ error: "User not found" });
    }
    res.json(user);
  } catch (error) {
    console.error(error.message);
    res.status(500).json({ error: "Internal Server Error" });
  }
});

// Update user by username
router.put("/:username", async (req, res) => {
  const { username } = req.params;
  const { email, password } = req.body;
  
  // Ensure that at least one field (email or password) is provided for update
  if (!email && !password) {
    return res.status(400).json({ error: "Invalid update data" });
  }

  try {
    const updatedUser = await UserModel.updateUser(username, email, password);
    if (!updatedUser) {
      return res.status(404).json({ error: "User not found" });
    }
    res.json({ message: "User updated successfully", user: updatedUser });
  } catch (error) {
    console.error(error.message);
    res.status(500).json({ error: "Internal Server Error" });
  }
});

// Delete user by username
router.delete("/:username", async (req, res) => {
  const { username } = req.params;
  try {
    const deletedUser = await UserModel.deleteUser(username);
    if (!deletedUser) {
      return res.status(404).json({ error: "User not found" });
    }
    res.json({ message: "User deleted successfully", user: deletedUser });
  } catch (error) {
    console.error(error.message);
    res.status(500).json({ error: "Internal Server Error" });
  }
});

module.exports = router;
