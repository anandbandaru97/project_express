const sqlite3 = require("sqlite3");
const db = new sqlite3.Database("database.db");

db.run(`
    CREATE TABLE IF NOT EXISTS tasks (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      title TEXT NOT NULL,
      status TEXT DEFAULT 'not done',
      project_id INTEGER NOT NULL,
      FOREIGN KEY (project_id) REFERENCES projects(username)
    )
  `);

// Create a To-Do (Create)
function createToDoAsync(title, status, project_id) {
  return new Promise((resolve, reject) => {
    const query = "INSERT INTO tasks (title, status, project_id) VALUES (?, ?, ?)";
    db.run(query, [title, status, project_id], function (err) {
      if (err) {
        reject(err);
      }
      resolve(this.lastID);
    });
  });
}

// Read To-Do List (Read All To-Do Items)
function getAllToDosAsync() {
  return new Promise((resolve, reject) => {
    const query = "SELECT * FROM tasks";
    db.all(query, (err, rows) => {
      if (err) {
        reject(err);
      }
      resolve(rows);
    });
  });
}

// Read a Single To-Do Item (Read One To-Do Item by ID)
function getToDoByIdAsync(id) {
  return new Promise((resolve, reject) => {
    const query = "SELECT * FROM tasks WHERE id = ?";
    db.get(query, [id], (err, row) => {
      if (err) {
        reject(err);
      }
      resolve(row);
    });
  });
}

// Update a To-Do (Update)
function updateToDoAsync(id, title, status, project_id) {
  return new Promise((resolve, reject) => {
    const query = "UPDATE tasks SET title = ?, status = ?, project_id = ? WHERE id = ?";
    db.run(query, [title, status, project_id, id], function (err) {
      if (err) {
        reject(err);
      }
      // Check if any rows were affected
      if (this.changes === 0) {
        reject(new Error("To-Do not found"));
      }
      resolve();
    });
  });
}

// Delete a To-Do (Delete)
function deleteToDoAsync(id) {
  return new Promise((resolve, reject) => {
    const query = "DELETE FROM tasks WHERE id = ?";
    db.run(query, [id], function (err) {
      if (err) {
        reject(err);
      }
      // Check if any rows were affected
      if (this.changes === 0) {
        reject(new Error("To-Do not found"));
      }
      resolve();
    });
  });
}

module.exports = {
  createToDoAsync,
  getAllToDosAsync,
  getToDoByIdAsync,
  updateToDoAsync,
  deleteToDoAsync,
};
