const sqlite3 = require("sqlite3");
const db = new sqlite3.Database("database.db");

db.run(`
CREATE TABLE IF NOT EXISTS users (
  username INTEGER PRIMARY KEY AUTOINCREMENT,
  username TEXT UNIQUE NOT NULL,
  email TEXT NOT NULL,
  password TEXT NOT NULL
)
`);

function createUser(username, email, password) {
  return new Promise((resolve, reject) => {
    db.run(
      "INSERT INTO users (username, email, password) VALUES (?, ?, ?)",
      [username, email, password],
      function (err) {
        if (err) {
          reject(err);
        }
        resolve(this.lastID);
      }
    );
  });
}

function getAllUsers() {
  return new Promise((resolve, reject) => {
    db.all("SELECT * FROM users", (err, rows) => {
      if (err) {
        reject(err);
      }
      resolve(rows);
    });
  });
}

function getUserByUsername(username) {
  return new Promise((resolve, reject) => {
    db.get("SELECT * FROM users WHERE username = ?", [username], (err, row) => {
      if (err) {
        reject(err);
      }
      resolve(row);
    });
  });
}

// Update user by username
function updateUser(username, email, password) {
  return new Promise((resolve, reject) => {
    db.run(
      "UPDATE users SET email = ?, password = ? WHERE username = ?",
      [email, password, username],
      function (err) {
        if (err) {
          reject(err);
        }
        if (this.changes === 0) {
          resolve(null); // No rows were affected, user not found
        } else {
          resolve({ username, email, password });
        }
      }
    );
  });
}

// Delete user by username
function deleteUser(username) {
  return new Promise((resolve, reject) => {
    db.get("SELECT * FROM users WHERE username = ?", [username], (err, row) => {
      if (err) {
        reject(err);
      }
      if (!row) {
        resolve(null); // User not found
      } else {
        db.run("DELETE FROM users WHERE username = ?", [username], (err) => {
          if (err) {
            reject(err);
          }
          resolve(row); // Return the deleted user
        });
      }
    });
  });
}

module.exports = {
  createUser,
  getAllUsers,
  getUserByUsername,
  updateUser,
  deleteUser,
};
