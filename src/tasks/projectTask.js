const sqlite3 = require("sqlite3");
const db = new sqlite3.Database("database.db");

db.run(`
    CREATE TABLE IF NOT EXISTS projects (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      title TEXT NOT NULL,
      description TEXT,
      status TEXT DEFAULT 'not done',
      user_id INTEGER NOT NULL,
      FOREIGN KEY (user_id) REFERENCES users(username)
    )
  `);

function createProject(title, description, user_id) {
  return new Promise((resolve, reject) => {
    db.run(
      "INSERT INTO projects (title, description, user_id) VALUES (?, ?, ?)",
      [title, description, user_id],
      function (err) {
        if (err) {
          reject(err);
        }
        resolve(this.lastID);
      }
    );
  });
}

function getProjectById(projectId) {
  return new Promise((resolve, reject) => {
    db.get("SELECT * FROM projects WHERE id = ?", [projectId], (err, row) => {
      if (err) {
        reject(err);
      }
      resolve(row);
    });
  });
}

function getAllProjects() {
  return new Promise((resolve, reject) => {
    const query = "SELECT * FROM projects";
    db.all(query, (err, rows) => {
      if (err) {
        reject(err);
      }
      resolve(rows);
    });
  });
}

function updateProject(projectId, title, description) {
  return new Promise((resolve, reject) => {
    db.run(
      "UPDATE projects SET title = ?, description = ? WHERE id = ?",
      [title, description, projectId],
      (err) => {
        if (err) {
          reject(err);
        }
        resolve();
      }
    );
  });
}

function deleteProject(projectId) {
  return new Promise((resolve, reject) => {
    db.run("DELETE FROM projects WHERE username = ?", [projectId], (err) => {
      if (err) {
        reject(err);
      }
      resolve();
    });
  });
}

module.exports = {
  createProject,
  getProjectById,
  getAllProjects,
  updateProject,
  deleteProject,
};
